
#include <iostream>
#include "e/e.h"
#include "d/d.h"

namespace e {
  void addIndentation(uint indentation) {
    for (uint i = 0; i < indentation; ++i) {
      std::cout << "  ";
    }
  }

  void doThingE(uint indentation) {
    addIndentation(indentation);
    std::cout << "this is thing E" << std::endl;

    d::doThingD(indentation + 1);
  };
}
